﻿using O_desc.Models;
using System;

namespace O_desc.Controls
{
    class Timing
    {
        static public string LastAccess(PhysicalThing thing)
        {
            const string baseText = "last access : ";
            var access = thing.LastAccess;
            var now = DateTime.Now;

            var delta = now.Year - access.Year;
            var unit = "year";
            if (delta == 0)
            {
                delta = now.Month - access.Month;
                unit = "month";
                if (delta == 0)
                {
                    delta = now.DayOfYear - access.DayOfYear;
                    unit = "day";
                    if (delta == 0)
                    {
                        delta = now.Hour - access.Hour;
                        unit = "hour";
                        if (delta == 0)
                        {
                            delta = now.Minute - access.Minute;
                            unit = "minute";
                            if (delta == 0)
                            {
                                return baseText + "less than a minute";
                            }
                        }
                    }
                }
            }

            if (delta > 1)
            {
                unit += "s";
            }

            return baseText + delta + " " + unit;
        }
    }
}
