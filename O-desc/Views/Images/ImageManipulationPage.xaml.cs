﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Shell;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.Images
{
    public partial class ImageManipulationPage
    {
        public AnnotationLocated CurrentAnnotation;
        private bool _isEditing;

        public ImageManipulationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            PhotoPoint.Visibility = Visibility.Collapsed;
            base.OnNavigatedTo(e);

            _isEditing = CurrentAnnotation != null;

            if (!_isEditing)
            {
                CurrentAnnotation = new AnnotationLocated();
                var saveButton = new ApplicationBarIconButton(new Uri("/Images/check.png", UriKind.Relative)) { Text = "validate" };
                saveButton.Click += SaveButton_Click;
                ApplicationBar.Buttons.Add(saveButton);
                ApplicationBar.IsVisible = true;
            }
            else
            {
                ApplicationBar.IsVisible = false;
            }
            DisplayTheCross();
            NoteTextBox.DataContext = CurrentAnnotation;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();

            ((Photo) PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing).LocatedAnnotations.Add(CurrentAnnotation);
#pragma warning disable 4014
            PhysicalThing.Save(PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);
#pragma warning restore 4014
        }

        private void PhotoSpot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (CurrentAnnotation == null) return;

            var top = e.GetPosition(PhotoSpot).Y - (PhotoPoint.ActualHeight / 2);
            var left = e.GetPosition(PhotoSpot).X - (PhotoPoint.ActualWidth / 2);

            if (e.GetPosition(PhotoSpot).Y > PhotoSpot.ActualHeight)
            {
                top = PhotoSpot.ActualHeight - (PhotoPoint.ActualHeight / 2);
            }
            if (e.GetPosition(PhotoSpot).Y < 0)
            {
                top = 0 - (PhotoPoint.ActualHeight / 2);
            }
            if (e.GetPosition(PhotoSpot).X > PhotoSpot.ActualWidth)
            {
                left = PhotoSpot.ActualWidth - (PhotoPoint.ActualWidth / 2);
            }
            if (e.GetPosition(PhotoSpot).X < 0)
            {
                left = 0 - (PhotoPoint.ActualWidth / 2);
            }

            CurrentAnnotation.PositionY = top / PhotoSpot.ActualHeight;
            CurrentAnnotation.PositionX = left / PhotoSpot.ActualWidth;

            DisplayTheCross();
        }

        private void DisplayTheCross()
        {
            if (CurrentAnnotation.PositionX == -1 || CurrentAnnotation.PositionY == -1) return;


            var top = CurrentAnnotation.PositionY * PhotoSpot.ActualHeight;
            var left = CurrentAnnotation.PositionX * PhotoSpot.ActualWidth;

            var extraLeftMargin = (PageContent.ActualWidth - PhotoSpot.ActualWidth) / 2;
            //var extraTopMargin = (PageContent.ActualHeight - PhotoSpot.ActualHeight) / 2;

            PhotoPoint.Margin = new Thickness(left + extraLeftMargin, top, 0, 0);

            PhotoPoint.Visibility = Visibility.Visible;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
#pragma warning disable 4014
            PhysicalThing.Save(PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);
#pragma warning restore 4014
        }

        private void PhotoSpot_OnLoaded(object sender, RoutedEventArgs e)
        {
            DisplayTheCross();
            PhotoSpot.MaxHeight = PhotoSpot.ActualHeight;
        }
    }
}