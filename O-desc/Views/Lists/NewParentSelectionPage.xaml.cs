﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls;
using System.Windows.Navigation;
using O_desc.Models;

namespace O_desc.Views.Lists
{
    public partial class NewParentSelectionPage
    {
        [DataMember]
        private ObservableCollection<PhysicalThing> _parents;
        public ObservableCollection<PhysicalThing> Parents
        {
            get
            {
                _parents = new ObservableCollection<PhysicalThing>();
                var dataContext = (PhysicalThing)DataContext;
                foreach (var rootContext in MainPage.TheMainPage.RootContexts)
                {
                    if (rootContext.UniqueId == dataContext.UniqueId) continue;
                    
                    _parents.Add(rootContext);
                    rootContext.AddChildrenToList(ref _parents, ref dataContext);
                }
                
                for (var index = _parents.Count -1; index >= 0; index--)
                {
                    var parent = _parents[index];
                    if (parent.GetType() != typeof(RealContext))
                    {
                        _parents.RemoveAt(index);
                    }

                    if (parent.IsInParents(dataContext))
                    {
                        _parents.RemoveAt(index);
                    }

                    if (parent.UniqueId == dataContext.UniqueId)
                    {
                        _parents.RemoveAt(index);
                    }
                    else if (dataContext.Parent != null)
                    {
                        if (parent.UniqueId == dataContext.Parent.UniqueId)
                        {
                            _parents.RemoveAt(index);
                        }
                    }

                    foreach (var child in dataContext.Children)
                    {
                        if (child.UniqueId == parent.UniqueId)
                        {
                            _parents.RemoveAt(index);
                        }
                    }
                }
                return _parents;
            }
            set { ChangeProperty(ref _parents, value); }
        }

        public NewParentSelectionPage()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] string propertyName = "")
        {
            if (Equals(attribute, value)) return;
            attribute = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(propertyName);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ParentList.ItemsSource = Parents;
        }

        private void ParentList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ((PhysicalThing) DataContext).MoveUnder(((PhysicalThing) ParentList.SelectedItem));
            PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing =
                ((PhysicalThing) ParentList.SelectedItem);

            NavigationService.GoBack();
        }
    }
}