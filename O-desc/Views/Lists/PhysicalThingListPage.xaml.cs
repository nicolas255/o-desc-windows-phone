﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Windows.Storage;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using O_desc.Models;
using O_desc.Views.Edit;
using O_desc.Views.Images;
using O_desc.Views.New;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace O_desc.Views.Lists
{
    public partial class PhysicalThingListPage : INotifyPropertyChanged
    {
        private bool _choosingAnnotationType;
        private const string NoDataInField = "-";
        public bool AutoGo;
        public bool AutoGoAndEdit;

        protected CameraCaptureTask CameraCaptureTask;

        public static PhysicalThingListPage ThePhysicalThingListPage;

        private PhysicalThing _currentPhysicalThing;

        public PhysicalThing CurrentPhysicalThing
        {
            get { return _currentPhysicalThing; }
            set { ChangeProperty(ref _currentPhysicalThing, value); }
        }
        

        private ObservableCollection<PhysicalThing> _printedPhysicalThings;
        public ObservableCollection<PhysicalThing> PrintedPhysicalThings
        {
            get {
                return _printedPhysicalThings ?? (_printedPhysicalThings = new ObservableCollection<PhysicalThing>());
            }
        }

        private ObservableCollection<Annotation> _printedAnnotations;

        public ObservableCollection<Annotation> PrintedAnnotations
        {
            get { return _printedAnnotations ?? (_printedAnnotations = new ObservableCollection<Annotation>()); }
        }

        public PhysicalThingListPage()
        {
            InitializeComponent();

            ThePhysicalThingListPage = this;

            DataContext = this;

            CameraCaptureTask = new CameraCaptureTask();
            CameraCaptureTask.Completed += cameraCaptureTask_Completed;
        }

        private async void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult != TaskResult.OK) return;

            var photo = new Photo();
            var bmp = new BitmapImage();
            bmp.SetSource(e.ChosenPhoto);
            var wbmp = new WriteableBitmap(bmp);

            if (!await SaveImageLocalStorage(wbmp, photo)) return;

            photo.Bitmap = bmp;
            photo.Parent = CurrentPhysicalThing;
            photo.DateCreation = DateTime.Now;
            photo.LastAccess = DateTime.Now;
            photo.Name = photo.DateCreationString;
            photo.Contact = photo.Parent.Contact;
            CurrentPhysicalThing.Children.Add(photo);
            CurrentPhysicalThing.ChildrenReferences.Add(photo.UniqueId);

#pragma warning disable 4014
            PhysicalThing.Save(photo);
            PhysicalThing.Save(CurrentPhysicalThing);
#pragma warning restore 4014

            Filter();

            AutoGoAndEdit = true;
            PtList.SelectedIndex = 0;
        }

        public void Filter()
        {
            GenerateList();
            AdaptLayout();
            AdaptAppBarButtons();
            ConnectAdditionnalData();
        }

        private void GenerateList()
        {
            PrintedPhysicalThings.Clear();
            PrintedAnnotations.Clear();

            if (CurrentPhysicalThing.GetType() == typeof (Photo))
            {
                var listAnnotationsLocated = ((Photo) CurrentPhysicalThing).LocatedAnnotations;
                foreach (var annotation in listAnnotationsLocated)
                {
                    _printedAnnotations.Add(annotation);
                }
                var listAnnotations = ((Photo) CurrentPhysicalThing).Annotations;
                foreach (var annotation in listAnnotations)
                {
                    _printedAnnotations.Add(annotation);
                }

                var listAnnotationsTmp = _printedAnnotations.OrderBy(o => o.DateCreation).Reverse().ToList();
                _printedAnnotations.Clear();
                foreach (var annotation in listAnnotationsTmp)
                {
                    _printedAnnotations.Add(annotation);
                }
            }
            else
            {
                var listPt = CurrentPhysicalThing.Children.OrderBy(o => o.LastAccess).Reverse().ToList();
                foreach (var item in listPt.Where(item => item.Name.ToLower().Contains(SearchTextBox.Text.ToLower())))
                {
                    _printedPhysicalThings.Add(item);
                }
            }
            
        }

        private void ConnectAdditionnalData()
        {
            DescriptionSpot.Text = NoDataInField;

            if (CurrentPhysicalThing.GetType() == typeof(Photo))
            {
                PhotoSpot.Source = ((Photo)CurrentPhysicalThing).Bitmap;
            }
            if (CurrentPhysicalThing.GetType() == typeof(RealObject))
            {
                if (((RealObject)CurrentPhysicalThing).Description.Length > 0)
                {
                    DescriptionSpot.Text = ((RealObject)CurrentPhysicalThing).Description;
                }
            }
            if (CurrentPhysicalThing.GetType() == typeof(RealContext))
            {
                var context = (RealContext) CurrentPhysicalThing;

                CheckForEmptyField(ref DescriptionSpot, context.Description);

                CheckForEmptyField(ref StreetLine1TextBlock, context.Address.StreetLine1);
                CheckForEmptyField(ref StreetLine2TextBlock, context.Address.StreetLine2);
                CheckForEmptyField(ref CityTextBlock, context.Address.City);
                CheckForEmptyField(ref CountryTextBlock, context.Address.Country);
                CheckForEmptyField(ref ZipCodeTextBlock, context.Address.ZipCode);

            }

            CheckForEmptyField(ref PhoneNumberTextBlock, CurrentPhysicalThing.Contact.Phone);
            CheckForEmptyField(ref EmailTextBlock, CurrentPhysicalThing.Contact.Mail);
            CheckForEmptyField(ref FirstNameTextBlock, CurrentPhysicalThing.Contact.NameFirst);
            CheckForEmptyField(ref LastNameTextBlock, CurrentPhysicalThing.Contact.NameLast);
            
            MainPivot.Title = CurrentPhysicalThing.Name.ToLower() + "/";

        }

        //Check if the field is empty. Then set the field text or the "NoData" string depending on the result
        private static void CheckForEmptyField(ref TextBlock textBlock, string field)
        {
            textBlock.Text = field.Length > 0 ? field : NoDataInField;
        }

        private void AdaptLayout()
        {
            LoadingProgressBar.Visibility = Visibility.Visible;
            DescriptionSpotLabel.Visibility = Visibility.Collapsed;
            DescriptionSpot.Visibility = Visibility.Collapsed;
            NothingMessageTextBlock.Visibility = Visibility.Collapsed;
            PhotoSpotBorder.Visibility = Visibility.Collapsed;
            PtList.Visibility = Visibility.Collapsed;
            AnnotationsList.Visibility = Visibility.Collapsed;
            AddressSpot.Visibility = Visibility.Collapsed;
            PhotoPoint.Visibility = Visibility.Collapsed;

            if (CurrentPhysicalThing.GetType() == typeof(RealObject))
            {
                DescriptionSpotLabel.Visibility = Visibility.Visible;
                DescriptionSpot.Visibility = Visibility.Visible;
            }
            if (CurrentPhysicalThing.GetType() == typeof(RealContext))
            {
                DescriptionSpotLabel.Visibility = Visibility.Visible;
                DescriptionSpot.Visibility = Visibility.Visible;
                AddressSpot.Visibility = Visibility.Visible;
            }
            if (CurrentPhysicalThing.GetType() == typeof(Photo))
            {
                PhotoSpotBorder.Visibility = Visibility.Visible;
            }

            if (CurrentPhysicalThing.Children.Count > 8)
            {
                SearchTextBox.Visibility = Visibility.Visible;
            }
            else
            {
                SearchTextBox.Visibility = Visibility.Collapsed;
                SearchTextBox.Text = "";
            }

            LoadingProgressBar.Visibility = Visibility.Collapsed;

            if (_printedPhysicalThings.Count > 0)
            {
                PtList.Visibility = Visibility.Visible;
            }
            else if (_printedAnnotations.Count > 0)
            {
                AnnotationsList.Visibility = Visibility.Visible;
            }
            else
            {
                NothingMessageTextBlock.Visibility = Visibility.Visible;
            }
        }

        private void AdaptAppBarButtons()
        {
            ApplicationBar.Buttons.Clear();
            ApplicationBar.MenuItems.Clear();

            var editButton = new ApplicationBarMenuItem("edit " + CurrentPhysicalThing.Name);
            editButton.Click += edit_button_Click;
            ApplicationBar.MenuItems.Add(editButton);

            var deleteButton = new ApplicationBarMenuItem("delete " + CurrentPhysicalThing.Name);
            deleteButton.Click += DeleteButton_Click;
            ApplicationBar.MenuItems.Add(deleteButton);

            if (CurrentPhysicalThing.Parent != null && (CurrentPhysicalThing.GetType() == typeof (RealObject) || CurrentPhysicalThing.GetType() == typeof(RealContext)))
            {
                var moveButton = new ApplicationBarMenuItem("move " + CurrentPhysicalThing.Name);
                moveButton.Click += MoveButton_Click;
                ApplicationBar.MenuItems.Add(moveButton);
            }
            
            if (MainPivot.SelectedIndex == 0)
            {
                if (CurrentPhysicalThing.GetType() == typeof(RealObject))
                {
                    var cameraButton = new ApplicationBarIconButton(new Uri("/Images/camera.png", UriKind.Relative)) { Text = "new photo" };
                    cameraButton.Click += camera_button_Click;
                    ApplicationBar.Buttons.Add(cameraButton);
                }
                if (CurrentPhysicalThing.GetType() == typeof(RealContext))
                {
                    var newItemButton = new ApplicationBarIconButton(new Uri("/Images/add.png", UriKind.Relative)) { Text = "new item" };
                    newItemButton.Click += new_item_button_Click;
                    ApplicationBar.Buttons.Add(newItemButton);
                }
                if (CurrentPhysicalThing.GetType() == typeof(Photo))
                {
                    var newAnnotationButton = new ApplicationBarIconButton(new Uri("/Images/add_note.png", UriKind.Relative)) { Text = "new note" };
                    newAnnotationButton.Click += newAnnotationButton_button_Click;
                    ApplicationBar.Buttons.Add(newAnnotationButton);
                }
            }

            if (AnnotationsList.SelectedIndex != -1)
            {
                var annotationEditButton = new ApplicationBarIconButton(new Uri("/Images/edit.png", UriKind.Relative)) { Text = "edit note" };
                annotationEditButton.Click += annotationEditButton_Click;
                ApplicationBar.Buttons.Add(annotationEditButton);

                var annotationDeleteButton = new ApplicationBarIconButton(new Uri("/Images/delete.png", UriKind.Relative)) { Text = "delete note" };
                annotationDeleteButton.Click += annotationDeleteButton_Click;
                ApplicationBar.Buttons.Add(annotationDeleteButton);
            }

            ApplicationBar.Mode = ApplicationBar.Buttons.Count < 1 ? ApplicationBarMode.Minimized : ApplicationBarMode.Default;
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Lists/NewParentSelectionPage.xaml", UriKind.Relative));
        }

        private void annotationDeleteButton_Click(object sender, EventArgs e)
        {
            for (var index = 0; index < ((Photo) CurrentPhysicalThing).Annotations.Count; index++)
            {
                var annotation = ((Photo) CurrentPhysicalThing).Annotations[index];
                if (annotation.Note != ((Annotation) AnnotationsList.SelectedItem).Note) continue;
                ((Photo) CurrentPhysicalThing).Annotations.RemoveAt(index);
                Filter();
#pragma warning disable 4014
                PhysicalThing.Save(CurrentPhysicalThing);
#pragma warning restore 4014
                return;
            }

            for (var index = 0; index < ((Photo) CurrentPhysicalThing).LocatedAnnotations.Count; index++)
            {
                var locatedAnnotation = ((Photo) CurrentPhysicalThing).LocatedAnnotations[index];
                if (locatedAnnotation.Note != ((Annotation) AnnotationsList.SelectedItem).Note) continue;
                ((Photo)CurrentPhysicalThing).LocatedAnnotations.RemoveAt(index);
                Filter();
#pragma warning disable 4014
                PhysicalThing.Save(CurrentPhysicalThing);
#pragma warning restore 4014
                return;
            }
        }

        private void annotationEditButton_Click(object sender, EventArgs e)
        {
            if (AnnotationsList.SelectedItem.GetType() == typeof(AnnotationLocated))
            {
                NavigationService.Navigate(new Uri("/Views/Images/ImageManipulationPage.xaml", UriKind.Relative));
            }
            if (AnnotationsList.SelectedItem.GetType() == typeof(Annotation))
            {
                NavigationService.Navigate(new Uri("/Views/Edit/AnnotationEditPage.xaml", UriKind.Relative));
            }
        }

        public void ForceBackToList()
        {
            MainPivot.SelectedIndex = 0;
        }

        private void camera_button_Click(object sender, EventArgs e)
        {
            CameraCaptureTask.Show();
        }

        private static async Task<bool> SaveImageLocalStorage(WriteableBitmap bmp, PhysicalThing photo)
        {
            var folder = ApplicationData.Current.LocalFolder;
            var file = await folder.CreateFileAsync(photo.UniqueId + ".jpg", CreationCollisionOption.ReplaceExisting);


            if (file == null) return true;
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = storage.CreateFile(photo.UniqueId + ".jpg"))
                {
                    bmp.SaveJpeg(stream, bmp.PixelWidth, bmp.PixelHeight, 0, 100);
                    stream.Close();
                }
            }
            return true;
        }

        private void edit_button_Click(object sender, EventArgs e)
        {
            if (CurrentPhysicalThing.GetType() == typeof(RealContext))
            {
                NavigationService.Navigate(new Uri("/Views/Edit/ContextEditPage.xaml", UriKind.Relative));
            }
            if (CurrentPhysicalThing.GetType() == typeof(RealObject))
            {
                NavigationService.Navigate(new Uri("/Views/Edit/ObjectEditPage.xaml", UriKind.Relative));
            }
            if (CurrentPhysicalThing.GetType() == typeof(Photo))
            {
                NavigationService.Navigate(new Uri("/Views/Edit/PhotoEditPage.xaml", UriKind.Relative));
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var p = (PhoneApplicationPage)e.Content;
            if (p != null)
            {

                if (p.GetType() != typeof(MainPage)) p.DataContext = CurrentPhysicalThing;

                if (p.GetType() == typeof(PhysicalThingNewPage))
                {
                    ((PhysicalThingNewPage)p).CallingPage = this;
                }
                if (p.GetType() == typeof(ContextEditPage) || p.GetType() == typeof(ObjectEditPage) || p.GetType() == typeof(PhotoEditPage))
                {
                    if (p.GetType() == typeof(PhotoEditPage) && AutoGoAndEdit)
                    {
                        ((PhotoEditPage) p).Preselect = true;
                        AutoGoAndEdit = false;
                    }
                }
                if (p.GetType() == typeof(ImageManipulationPage))
                {
                    ((ImageManipulationPage)p).CurrentAnnotation = (AnnotationLocated)AnnotationsList.SelectedItem;
                }
                if (p.GetType() == typeof(AnnotationEditPage))
                {
                    ((AnnotationEditPage)p).CurrentAnnotation = (Annotation)AnnotationsList.SelectedItem;
                }
            }
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Focus();
            PtList.SelectedIndex = -1;
            Filter();
            
            base.OnNavigatedTo(e);

            if (AutoGo)
            {
                AutoGo = false;
                PtList.SelectedIndex = 0;
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Filter();
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Focus();
            }
        }

        private void new_item_button_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/New/PhysicalThingNewPage.xaml", UriKind.Relative));
        }

        private void newAnnotationButton_button_Click(object sender, EventArgs e)
        {
            var messageBox = new CustomMessageBox()
            {
                Caption = "New annotation",
                Message = "What kind of annotation would you like to add ?",
                LeftButtonContent = "Simple",
                RightButtonContent = "Located"
            };

            messageBox.Dismissed += (s1, e1) =>
            {
                _choosingAnnotationType = false;

                switch (e1.Result)
                {
                    //Simple annotation
                    case CustomMessageBoxResult.LeftButton:
                        AnnotationsList.SelectedIndex = -1;
                        NavigationService.Navigate(new Uri("/Views/New/AnnotationNewPage.xaml", UriKind.Relative));
                        break;
                    //Located annotation
                    case CustomMessageBoxResult.RightButton:
                        AnnotationsList.SelectedIndex = -1;
                        NavigationService.Navigate(new Uri("/Views/Images/ImageManipulationPage.xaml", UriKind.Relative));
                        break;
                }
            };

            messageBox.Show();
            _choosingAnnotationType = true;
        }

        private void PtList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Focus();

            if (PtList.SelectedItem == null) return;
            CurrentPhysicalThing = (PhysicalThing)PtList.SelectedItem;
            ((PhysicalThing)PtList.SelectedItem).LastAccess = DateTime.Now;
#pragma warning disable 4014
            PhysicalThing.Save((PhysicalThing)PtList.SelectedItem);
#pragma warning restore 4014

            Filter();

            if (AutoGoAndEdit)
            {
                MainPivot.SelectedIndex = 1;
                edit_button_Click(this, new EventArgs());
            }
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            //Check if the choosing annotation type dialog is shown. If it's the case, do nothing because otherwise, it will go back to the parent view while the dialog is already closed.
            if (_choosingAnnotationType) return;
            if (CurrentPhysicalThing.Parent == null)
            {
                base.OnBackKeyPress(e);
            }
            else
            {
                if (MainPivot.SelectedIndex == 1)
                {
                    MainPivot.SelectedIndex = 0;
                }
                CurrentPhysicalThing = CurrentPhysicalThing.Parent;
                Filter();
                e.Cancel = true;
            }
        }

        private async void DeleteButton_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show("That operation cannot be undone. Are you sure ?",
                    "Removing " + CurrentPhysicalThing.Name, MessageBoxButton.OKCancel) != MessageBoxResult.OK) return;
            await PhysicalThing.Delete(CurrentPhysicalThing);
            if ((CurrentPhysicalThing).Parent == null)
            {
                MainPage.TheMainPage.RootContexts.Remove((RealContext)CurrentPhysicalThing);
                NavigationService.GoBack();
            }
            else
            {
                CurrentPhysicalThing.Parent.Children.Remove(CurrentPhysicalThing);
                OnBackKeyPress(new CancelEventArgs());
                ForceBackToList();
            }
        }

        private void PhotoSpotBorder_Tap(object sender, GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Images/ImageViewerPage.xaml", UriKind.Relative));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] string propertyName = "")
        {
            if (Equals(attribute, value)) return;
            attribute = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(propertyName);
        }

        private void PhoneNumberTextBlock_OnTap(object sender, GestureEventArgs e)
        {
            if (PhoneNumberTextBlock.Text == "-") return;
            var phoneCallTask = new PhoneCallTask
            {
                PhoneNumber = PhoneNumberTextBlock.Text,
                DisplayName = LastNameTextBlock.Text + " " + FirstNameTextBlock.Text
            };
            phoneCallTask.Show();
        }

        private void EmailTextBlock_OnTap(object sender, GestureEventArgs e)
        {
            if (EmailTextBlock.Text == "-") return;
            var emailComposeTask = new EmailComposeTask { To = EmailTextBlock.Text };
            emailComposeTask.Show();
        }

        private void MainPivot_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }

        private void AnnotationsList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AdaptAppBarButtons();
            PhotoPoint.Visibility = Visibility.Collapsed;

            if(AnnotationsList.SelectedIndex == -1) return;
            if(typeof(AnnotationLocated) != AnnotationsList.SelectedItem.GetType()) return;

            var selectedAnnotation = (AnnotationLocated) AnnotationsList.SelectedItem;

            if (!(selectedAnnotation.PositionX != -1 && selectedAnnotation.PositionY != -1)) return;

            var top = selectedAnnotation.PositionY * PhotoSpot.ActualHeight;
            var left = selectedAnnotation.PositionX * PhotoSpot.ActualWidth;

            var extraLeftMargin = (PhotoContainer.ActualWidth - PhotoSpot.ActualWidth) / 2;
            //var extraTopMargin = (PhotoContainer.ActualHeight - PhotoSpot.ActualHeight) / 2;

            PhotoPoint.Margin = new Thickness(left + extraLeftMargin, top, 0, 0);

            PhotoPoint.Visibility = Visibility.Visible;
        }
    }
}