﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Windows.Storage;
using Microsoft.Phone.Controls;
using O_desc.Models;
using O_desc.Views.Lists;
using O_desc.Views.New;

namespace O_desc.Views
{
    public partial class MainPage
    {
        //Starting point
        public MainPage()
        {
            InitializeComponent();
            _isLoading = true;
            InitializeUi();
            ReadJsonAsync();
            //DemoData();

            DataContext = this;

            TheMainPage = this;
        }

        private bool _isLoading;
        public static MainPage TheMainPage;

        private ObservableCollection<RealContext> _rootContexts;
        public ObservableCollection<RealContext> RootContexts
        {
            get
            {
                if (_rootContexts == null)
                {
                    _rootContexts = new ObservableCollection<RealContext>();
                }

                //Sort the list by context name
                //Could not find a better way to transform a list into an ObservableCollection without adding additional nugget package that will do the same so...
                var list = _rootContexts.OrderBy(o => o.LastAccess).Reverse().ToList();
                _rootContexts.Clear();
                foreach (var item in list)
                {
                    _rootContexts.Add(item);
                }

                return _rootContexts;
            }
            set
            {
                _rootContexts = value;
            }
        }

        public void Filter()
        {

            LoadingProgressBar.Visibility = Visibility.Visible;
            NothingMessageTextBlock.Visibility = Visibility.Collapsed;
            ContextsList.Visibility = Visibility.Collapsed;

            if (_printedRootContexts == null)
            {
                _printedRootContexts = new ObservableCollection<RealContext>();
            }
            else
            {
                _printedRootContexts.Clear();
            }
            if (_rootContexts != null)
            {
                if (_rootContexts.Count > 8)
                {
                    SearchTextBox.Visibility = Visibility.Visible;
                }
                else
                {
                    SearchTextBox.Visibility = Visibility.Collapsed;
                    SearchTextBox.Text = "";
                }
                foreach (var context in _rootContexts.OrderBy(o => o.LastAccess).Reverse().ToList().Where(context => context.Name.ToLower().Contains(SearchTextBox.Text.ToLower())))
                {
                    _printedRootContexts.Add(context);
                }
            }

            if (_printedRootContexts.Count > 0)
            {
                ContextsList.Visibility = Visibility.Visible;
            }
            else
            {
                if (RootContexts.Count != 0 && !_isLoading) NothingMessageTextBlock.Visibility = Visibility.Visible;
            }
            if (!_isLoading) LoadingProgressBar.Visibility = Visibility.Collapsed;
        }

        private ObservableCollection<RealContext> _printedRootContexts;
        public ObservableCollection<RealContext> PrintedRootContexts
        {
            get
            {
                Filter();
                return _printedRootContexts;
            }
        }

        protected RealContext CurrentContext;

        private void InitializeUi()
        {
            LoadingProgressBar.Visibility = Visibility.Visible;
            ContextsList.Visibility = Visibility.Collapsed;
            NothingMessageTextBlock.Visibility = Visibility.Collapsed;
        }

        private async void ReadJsonAsync()
        {
            if (_rootContexts != null)
            {
                if (_rootContexts.Count == 0)
                {
                    await LoadRoots();
                }
            }
            else
            {
                await LoadRoots();
            }

            /*
            String temp = JsonConvert.SerializeObject(context1);
            RealContext context2 = JsonConvert.DeserializeObject<RealContext>(temp);

            await PhysicalThing.save(context1);
             */
        }

        public async void DemoData()
        {

            _rootContexts = new ObservableCollection<RealContext>();

            var folder = ApplicationData.Current.LocalFolder;
            var allFiles = await folder.GetFilesAsync();
            foreach (var currentFile in allFiles)
            {
                currentFile.DeleteAsync();
            }

            var demoContextHome = new RealContext();
            // ReSharper disable once InconsistentNaming
            var demoContextCCI = new RealContext();

            demoContextHome.Name = "Maison";
            demoContextHome.Description = "Ma très jolie maison";
            demoContextHome.Contact.NameFirst = "Nicolas";
            demoContextHome.Contact.NameLast = "Klein";
            demoContextHome.Contact.Mail = "nicolas.klein255@gmail.com";
            demoContextHome.Contact.Phone = "0675754800";
            demoContextHome.Address.StreetLine1 = "42 rue d'ici";
            demoContextHome.Address.StreetLine2 = "";
            demoContextHome.Address.City = "Quelque part";
            demoContextHome.Address.ZipCode = "42000";
            demoContextHome.Address.Country = "France";

            demoContextHome.Children.Add(new RealContext());
            demoContextHome.Children.Add(new RealContext());
            demoContextHome.Children.Add(new RealContext());
            demoContextHome.Children.Add(new RealContext());
            demoContextHome.Children.Add(new RealContext());
            demoContextHome.Children.Add(new RealObject());
            demoContextHome.Children.Add(new RealContext());

            foreach (var child in demoContextHome.Children)
            {
                child.Parent = demoContextHome;
                if (typeof(RealContext) == child.GetType())
                {
                    ((RealContext) child).Address = ((RealContext) child.Parent).Address;
                }
                child.Contact = child.Parent.Contact;
                demoContextHome.ChildrenReferences.Add(child.UniqueId);
            }

            ((RealContext)demoContextHome.Children[0]).Name = "Cuisine";
            ((RealContext)demoContextHome.Children[0]).Description = "";

            ((RealContext)demoContextHome.Children[1]).Name = "Salle de bain";
            ((RealContext)demoContextHome.Children[1]).Description = "";

            ((RealContext)demoContextHome.Children[2]).Name = "Salon";
            ((RealContext)demoContextHome.Children[2]).Description = "";

            ((RealContext)demoContextHome.Children[3]).Name = "Chambre de gauche";
            ((RealContext)demoContextHome.Children[3]).Description = "";

            ((RealContext)demoContextHome.Children[4]).Name = "Chambre de droite";
            ((RealContext)demoContextHome.Children[4]).Description = "";

            ((RealObject)demoContextHome.Children[5]).Name = "Portail";
            ((RealObject)demoContextHome.Children[5]).Description = "";

            ((RealContext)demoContextHome.Children[6]).Name = "Cave";
            ((RealContext)demoContextHome.Children[6]).Description = "";

            demoContextCCI.Name = "Pôle formation";
            demoContextCCI.Description = "C'est là que ça se passe !";
            demoContextCCI.Contact.NameFirst = "Germain";
            demoContextCCI.Contact.NameLast = "Jeckert";
            demoContextCCI.Contact.Mail = "unemail@ccicolmar.fr";
            demoContextCCI.Contact.Phone = "0562145784";
            demoContextCCI.Address.StreetLine1 = "4 route du Rhin";
            demoContextCCI.Address.StreetLine2 = "";
            demoContextCCI.Address.City = "Colmar";
            demoContextCCI.Address.ZipCode = "68000";
            demoContextCCI.Address.Country = "France";

            demoContextCCI.Children.Add(new RealContext());

            foreach (var child in demoContextCCI.Children)
            {
                child.Parent = demoContextCCI;
                if (typeof(RealContext) == child.GetType())
                {
                    ((RealContext)child).Address = ((RealContext)child.Parent).Address;
                }
                child.Contact = child.Parent.Contact;
                demoContextCCI.ChildrenReferences.Add(child.UniqueId);
            }

            ((RealContext)demoContextCCI.Children[0]).Name = "Salle 241";
            ((RealContext)demoContextCCI.Children[0]).Description = "";

            await PhysicalThing.SaveRecursively(demoContextCCI);
            await PhysicalThing.SaveRecursively(demoContextHome);

            _rootContexts.Add(demoContextCCI);
            _rootContexts.Add(demoContextHome);

            _isLoading = false;
            Filter();
        }

        public async Task LoadRoots()
        {
            _rootContexts = new ObservableCollection<RealContext>();

            var folder = ApplicationData.Current.LocalFolder;
            var allFiles = await folder.GetFilesAsync();
            foreach (var currentFile in allFiles)
            {
                var currentId = currentFile.Name.Replace(".json", "").Replace(".jpg", "");
                var keys = currentId.Split(new[] { "&" }, StringSplitOptions.None);
                if (keys.Length == 2)
                {
                    if (!keys[1].Contains(typeof (RealContext).ToString())) continue;
                    var context = new RealContext {UniqueId = currentId};
                    context = (RealContext)await PhysicalThing.LoadRecursively(context);
                    if (context == null) continue;
                    if (context.ParentReference == null)
                    {
                        _rootContexts.Add(context);
                    }
                    else if (context.ParentReference == "null")
                    {
                        _rootContexts.Add(context);
                    }
                }
                else
                {
                    await currentFile.DeleteAsync();
                }
            }

            _isLoading = false;
            Filter();
        }

        private void NewContextButton_Click(object sender, EventArgs e)
        {
            CurrentContext = new RealContext
            {
                DateCreation = DateTime.Now,
                LastAccess = DateTime.Now
            };

            NavigationService.Navigate(new Uri("/Views/New/ContextNewPage.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var p = (PhoneApplicationPage)e.Content;
            if (p != null)
            {
                if (p.GetType() == typeof(PhysicalThingListPage))
                {
                    ((PhysicalThingListPage)p).CurrentPhysicalThing = CurrentContext;
                }
                if (p.GetType() == typeof(ContextNewPage))
                {
                    p.DataContext = CurrentContext;
                }
            }
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Filter();
            ContextsList.SelectedIndex = -1;
            base.OnNavigatedTo(e);
        }

        private void ContextsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentContext = (RealContext)ContextsList.SelectedItem;

            if (CurrentContext == null) return;
            
            CurrentContext.LastAccess = DateTime.Now;
#pragma warning disable 4014
            PhysicalThing.Save(CurrentContext);
#pragma warning restore 4014
            NavigationService.Navigate(new Uri("/Views/Lists/PhysicalThingListPage.xaml", UriKind.Relative));
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Filter();
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Focus();
            }
        }
    }
}