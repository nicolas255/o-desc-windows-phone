﻿using System;
using System.Windows.Navigation;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.New
{
    public partial class AnnotationNewPage
    {
        public AnnotationNewPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            NoteTextBox.Focus();
            ParentName.Text = PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing.Name.ToLower() + "/";
            base.OnNavigatedTo(e);
        }

        private void SaveButton_OnClick(object sender, EventArgs e)
        {
            NavigationService.GoBack();

            var parentPt = ((Photo)PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);

            parentPt.Annotations.Add(new Annotation()
            {
                Note = NoteTextBox.Text
            });

#pragma warning disable 4014
            PhysicalThing.Save(parentPt);
#pragma warning restore 4014
        }
    }
}