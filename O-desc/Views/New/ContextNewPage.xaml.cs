﻿using System;
using System.Windows.Navigation;
using O_desc.Models;

namespace O_desc.Views.New
{
    public partial class ContextNewPage
    {
        public ContextNewPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            NameTextBox.Focus();
            base.OnNavigatedTo(e);
        }

        private void SaveButton_OnClick(object sender, EventArgs e)
        {
            MainPage.TheMainPage.RootContexts.Add((RealContext)DataContext);
            NavigationService.GoBack();
            //Those lines are after the GoBack() because, the binding system only write the new data when the focus is lost
            //So I have to move this after in order to get the context name at the saving point
#pragma warning disable 4014
            PhysicalThing.Save(((RealContext)DataContext));
#pragma warning restore 4014
        }
    }
}