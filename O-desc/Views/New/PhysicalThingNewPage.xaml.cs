﻿using System;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.New
{
    public partial class PhysicalThingNewPage
    {
        public PhoneApplicationPage CallingPage;

        public PhysicalThingNewPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            NameTextBox.Focus();
            ParentName.Text = ((PhysicalThingListPage)CallingPage).CurrentPhysicalThing.Name.ToLower() + "/";
            base.OnNavigatedTo(e);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            PhysicalThingListPage.ThePhysicalThingListPage.AutoGo = true;
            NavigationService.GoBack();

            NameTextBox.Focus();
            PhysicalThing newPt;
            var parentPt = ((PhysicalThingListPage)CallingPage).CurrentPhysicalThing;
            if (((ListPickerItem) TypeListPicker.SelectedItem).Content.ToString() == "Object")
            {
                newPt = new RealObject();
            }
            else
            {
                newPt = new RealContext();
            }
            newPt.Name = NameTextBox.Text;
            newPt.DateCreation = DateTime.Now;
            newPt.LastAccess = DateTime.Now;
            newPt.Parent = parentPt;
            newPt.Contact = newPt.Parent.Contact;
            parentPt.Children.Add(newPt);
            parentPt.ChildrenReferences.Add(newPt.UniqueId);
#pragma warning disable 4014
            PhysicalThing.Save(newPt);
            PhysicalThing.Save(parentPt);
#pragma warning restore 4014
            ((PhysicalThingListPage)CallingPage).CurrentPhysicalThing = parentPt;
        }
    }
}