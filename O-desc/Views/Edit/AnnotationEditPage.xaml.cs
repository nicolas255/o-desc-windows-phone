﻿using System.Windows.Navigation;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.Edit
{
    public partial class AnnotationEditPage
    {
        public Annotation CurrentAnnotation;

        public AnnotationEditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            NoteTextBox.Focus();
            ParentName.Text = PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing.Name.ToLower() + "/";
            NoteTextBox.DataContext = CurrentAnnotation;
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
#pragma warning disable 4014
            PhysicalThing.Save(PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);
#pragma warning restore 4014
        }
    }
}