﻿using System;
using System.Windows;
using System.Windows.Navigation;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.Edit
{
    public partial class PhotoEditPage
    {
        private Contact _previousContact;
        public bool Preselect;

        public PhotoEditPage()
        {
            InitializeComponent();
        }



        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Focus();
            ParentName.Text = ((PhysicalThing) DataContext).Name.ToLower() + "/";
            base.OnNavigatedTo(e);
            _previousContact = ((PhysicalThing)DataContext).Contact;
            
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing.OnContactUpdate(_previousContact);
#pragma warning disable 4014
            PhysicalThing.Save(PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);
#pragma warning restore 4014
        }

        private void PhotoName_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (Preselect)
            {
                PhotoName.Focus();
                PhotoName.SelectAll();
            }
        }
    }
}