﻿using System.Windows.Navigation;
using O_desc.Models;
using O_desc.Views.Lists;

namespace O_desc.Views.Edit
{
    public partial class ContextEditPage
    {
        private Contact _previousContact;

        public ContextEditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Focus();
            ParentName.Text = ((PhysicalThing) DataContext).Name.ToLower() + "/";
            base.OnNavigatedTo(e);
            _previousContact = ((PhysicalThing) DataContext).Contact;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing.OnContactUpdate(_previousContact);
#pragma warning disable 4014
            PhysicalThing.Save(PhysicalThingListPage.ThePhysicalThingListPage.CurrentPhysicalThing);
#pragma warning restore 4014
        }
    }
}