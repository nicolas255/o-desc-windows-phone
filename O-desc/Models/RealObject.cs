﻿using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class RealObject : PhysicalThing
    {
        #region Properties definitions

        [DataMember]
        private string _description;
        public string Description
        {
            get { return _description ?? (_description = ""); }
            set { _description = value; }
        }

        #endregion
    }
}
