﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Windows.Storage;
using Newtonsoft.Json;
using O_desc.Controls;

namespace O_desc.Models
{
    [DataContract]
    public class PhysicalThing : INotifyPropertyChanged
    {
        #region Properties definitions

        [DataMember]
        private string _uniqueId;
        public string UniqueId
        {
            get { return _uniqueId; }
            set {
                ChangeProperty(ref _uniqueId, value);
            }
        }

        [DataMember]
        private string _name;
        public string Name
        {
            get { return _name ?? (_name = ""); }
            set {
                ChangeProperty(ref _name, value);
            }
        }

        [DataMember]
        private DateTime _dateCreation;
        public DateTime DateCreation
        {
            get { return _dateCreation; }
            set {
                ChangeProperty(ref _dateCreation, value);
            }
        }

        [DataMember]
        private DateTime _lastAccess;
        public DateTime LastAccess
        {
            get { return _lastAccess; }
            set {
                ChangeProperty(ref _lastAccess, value);
            }
        }

        [DataMember]
        private List<string> _childrenReferences;
        public List<string> ChildrenReferences
        {
            get { return _childrenReferences ?? (_childrenReferences = new List<string>()); }
            set {
                ChangeProperty(ref _childrenReferences, value);
            }
        }

        private List<PhysicalThing> _children;
        public List<PhysicalThing> Children
        {
            get { return _children ?? (_children = new List<PhysicalThing>()); }
            set {
                ChangeProperty(ref _children, value);
            }
        }

        [DataMember]
        private string _parentReference;
        public string ParentReference
        {
            get {
                return _parentReference;
            }
            set {
                ChangeProperty(ref _parentReference, value);
            }
        }

        private PhysicalThing _parent;
        public PhysicalThing Parent
        {
            get { return _parent; }
            set {
                ChangeProperty(ref _parent, value);
                ParentReference = _parent.UniqueId;
            }
        }

        public string LastAccessString
        {
            get { return Timing.LastAccess(this); }
        }

        public string DateCreationString
        {
            get { return DateCreation.ToShortDateString() + " " + DateCreation.ToLongTimeString(); }
        }

        [DataMember]
        private Contact _contact;
        public Contact Contact
        {
            get { return _contact ?? (_contact = new Contact()); }
            set
            {
                ChangeProperty(ref _contact, value);
            }
        }

        public Thickness ParentOffset {
            get
            {
                return new Thickness((ParentCount) * 20, 4, 0, 4);
            }
        }

        public int ParentCount
        {
            get
            {
                if (Parent != null)
                {
                    return Parent.ParentCount + 1;
                }
                return 1;
            }
        }



        #endregion

        public PhysicalThing()
        {
            GenerateUniqueId();
            Contact = new Contact();

            DateCreation = DateTime.Now;
            LastAccess = DateTime.Now;
        }

        public void GenerateUniqueId()
        {
            UniqueId = Guid.NewGuid().ToString().Replace("{", "").Replace("}", "") + "&" + GetType();
        }

        public void MoveUnder(PhysicalThing newParent)
        {
            Parent.Children.Remove(this);
            Parent.ChildrenReferences.Remove(UniqueId);
#pragma warning disable 4014
            Save(Parent);
#pragma warning restore 4014
            Parent = newParent;
            ParentReference = newParent.UniqueId;
            Parent.Children.Add(this);
            Parent.ChildrenReferences.Add(UniqueId);
#pragma warning disable 4014
            Save(this);
            Save(Parent);
#pragma warning restore 4014
        }

        public static async Task Save<T>(T thingToSave)
        {
            var thing = thingToSave as PhysicalThing;

            var folder = ApplicationData.Current.LocalFolder;
            if (thing != null)
            {
                var file = await folder.CreateFileAsync(thing.UniqueId + ".json", CreationCollisionOption.ReplaceExisting);

                using (var file2 = new StreamWriter(file.Path))
                {
                    var s = JsonConvert.SerializeObject(thing);
                    file2.WriteLine(s);
                }
            }
        }

        //use for the demo
        public static async Task SaveRecursively(PhysicalThing thingToSave)
        {
            await Save(thingToSave);
            foreach (var child in thingToSave.Children)
            {
                await SaveRecursively(child);
            }
        }

        public static async Task<T> Load<T>(T thingToLoad)
        {
            var thing = thingToLoad as PhysicalThing;
            var folder = ApplicationData.Current.LocalFolder;
            Stream file;
            string fileName = null;

            try
            {
                if (thing != null) fileName = thing.UniqueId;
            }
            catch (Exception)
            {
                throw new Exception("Object is not a PhysicalThing");
            }

            try
            {
                file = await folder.OpenStreamForReadAsync(fileName + ".json");
            }
            catch (Exception)
            {
                throw new FileNotFoundException();
            }

            using (var streamReader = new StreamReader(file))
            {
                file.Seek(0, SeekOrigin.Begin);

                return JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd());
            }
        }

        public static async Task<PhysicalThing> LoadRecursively(PhysicalThing thingToLoad)
        {
            PhysicalThing result = null;
            var keys = thingToLoad.UniqueId.Split(new[] { "&" }, StringSplitOptions.None);
            if (keys.Length == 2)
            {
                if (keys[1].Contains(typeof(RealContext).ToString()))
                {
                    var context = new RealContext {UniqueId = thingToLoad.UniqueId};
                    context = await Load(context);
                    if (context != null)
                    {
                        result = context;
                    }
                }
                if (keys[1].Contains(typeof(RealObject).ToString()))
                {
                    var realObject = new RealObject {UniqueId = thingToLoad.UniqueId};
                    realObject = await Load(realObject);
                    if (realObject != null)
                    {
                        result = realObject;
                    }
                }
                if (keys[1].Contains(typeof(Photo).ToString()))
                {
                    var photo = new Photo {UniqueId = thingToLoad.UniqueId};
                    photo = await Load(photo);
                    if (photo != null)
                    {
                        var folder = ApplicationData.Current.LocalFolder;
                        var picture = await folder.GetFileAsync(photo.UniqueId + ".jpg");

                        var fileStream = await picture.OpenAsync(FileAccessMode.Read);
                        var bitmapImage = new BitmapImage();
                        bitmapImage.SetSource(fileStream.AsStream());
                        photo.Bitmap = bitmapImage;
                        result = photo;
                    }
                }
            }

            if (result == null) return null;
            if (result.ChildrenReferences == null) return result;
            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var childReference in result.ChildrenReferences)
            {
                var tmpPt = new PhysicalThing {UniqueId = childReference};
                tmpPt = await LoadRecursively(tmpPt);
                if (tmpPt == null) continue;
                tmpPt.Parent = result;
                result.Children.Add(tmpPt);
            }
            return result;
        }

        public static async Task Delete<T>(T thingToLoad)
        {
            var thing = thingToLoad as PhysicalThing;
            var folder = ApplicationData.Current.LocalFolder;
            StorageFile file;
            StorageFile resourceFile = null;
            string fileName = null;

            try
            {
                if (thing != null) fileName = thing.UniqueId;
            }
            catch (Exception)
            {
                throw new Exception("Object is not a PhysicalThing");
            }

            try
            {
                file = await folder.GetFileAsync(fileName + ".json");
            }
            catch (Exception)
            {
                throw new FileNotFoundException();
            }

            try
            {
                if (thing != null && thing.GetType() == typeof(Photo))
                {
                    resourceFile = await folder.GetFileAsync(fileName + ".jpg");
                }
            }
            catch (Exception)
            {
                throw new FileNotFoundException();
            }

            if (resourceFile != null)
            {
                await resourceFile.DeleteAsync();
            }
            if (thing != null && thing.Children != null)
            {
                foreach (var child in thing.Children)
                {
                    await DeleteRecursively(child);
                }
            }
            if (thing != null && thing.Parent != null)
            {
                var tmpPt = (PhysicalThing)thing.Parent.MemberwiseClone();
                tmpPt.ChildrenReferences.Remove(thing.UniqueId);
                tmpPt.Children.Remove(thing);
                await Save(tmpPt);
            }
            await file.DeleteAsync();
        }

        public static async Task DeleteRecursively<T>(T thingToLoad)
        {
            var thing = thingToLoad as PhysicalThing;
            var folder = ApplicationData.Current.LocalFolder;
            StorageFile file;
            StorageFile resourceFile = null;
            string fileName = null;

            try
            {
                if (thing != null) fileName = thing.UniqueId;
            }
            catch (Exception)
            {
                throw new Exception("Object is not a PhysicalThing");
            }

            try
            {
                file = await folder.GetFileAsync(fileName + ".json");
            }
            catch (Exception)
            {
                throw new FileNotFoundException();
            }

            try
            {
                if (thing != null && thing.GetType() == typeof(Photo))
                {
                    resourceFile = await folder.GetFileAsync(fileName + ".jpg");
                }
            }
            catch (Exception)
            {
                throw new FileNotFoundException();
            }

            if (resourceFile != null)
            {
                await resourceFile.DeleteAsync();
            }
            if (thing != null && thing.Children != null)
            {
                foreach (var child in thing.Children)
                {
                    await DeleteRecursively(child);
                }
            }
            await file.DeleteAsync();
        }

        public override string ToString()
        {
            return Name;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] string propertyName = "")
        {
            if (Equals(attribute, value)) return;
            attribute = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(propertyName);
        }

        public async void OnContactUpdate(Contact oldContact)
        {
            foreach (var child in Children)
            {
                if (child.Contact.IsEmpty() || child.Contact.IsEqualTo(oldContact))
                {
                    child.Contact = Contact;
                    await Save(child);
                }
                child.OnContactUpdate(oldContact);
            }
        }

        public void AddChildrenToList(ref ObservableCollection<PhysicalThing> list)
        {
            foreach (var child in Children)
            {
                list.Add(child);
                child.AddChildrenToList(ref list);
            }
        }

        public void AddChildrenToList(ref ObservableCollection<PhysicalThing> list, ref PhysicalThing excluded)
        {
            foreach (var child in Children)
            {
                if (child.UniqueId == excluded.UniqueId) continue;
                
                list.Add(child);
                child.AddChildrenToList(ref list);
            }
        }

        public bool IsInParents(PhysicalThing parent)
        {
            if (Parent == null) return false;
            return Parent.UniqueId != parent.UniqueId && Parent.IsInParents(this);
        }
    }
}
