﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class Contact : INotifyPropertyChanged
    {
        #region Properties definitions

        [DataMember]
        private string _nameFirst;
        public string NameFirst
        {
            get { return _nameFirst ?? (_nameFirst = ""); }
            set
            {
                ChangeProperty(ref _nameFirst, value);
            }
        }

        [DataMember]
        private string _nameLast;
        public string NameLast
        {
            get { return _nameLast ?? (_nameLast = ""); }
            set
            {
                ChangeProperty(ref _nameLast, value);
            }
        }

        [DataMember]
        private string _phone;
        public string Phone
        {
            get { return _phone ?? (_phone = ""); }
            set
            {
                ChangeProperty(ref _phone, value);
            }
        }

        [DataMember]
        private string _mail;
        public string Mail
        {
            get { return _mail ?? (_mail = ""); }
            set
            {
                ChangeProperty(ref _mail, value);
            }
        }

        #endregion

        public Contact()
        {
            NameFirst = "";
            NameLast = "";
            Phone = "";
            Mail = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] string propertyName = "")
        {
            if (Equals(attribute, value)) return;
            attribute = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(propertyName);
        }

        public bool IsEmpty()
        {
            if (Mail.Length > 0) return false;
            if (NameFirst.Length > 0) return false;
            if (Phone.Length > 0) return false;
            if (NameLast.Length > 0) return false;

            return true;
        }

        public bool IsEqualTo(Contact parentOldContact)
        {
            bool isEqual = true;

            if (parentOldContact.NameLast != NameLast) isEqual = false;
            if (parentOldContact.NameFirst != NameFirst) isEqual = false;
            if (parentOldContact.Mail != Mail) isEqual = false;
            if (parentOldContact.Phone != Phone) isEqual = false;

            return isEqual;
        }
    }
}
