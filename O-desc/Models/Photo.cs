﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;

namespace O_desc.Models
{
    [DataContract]
    public class Photo : Resource
    {
        #region Properties definitions

        [DataMember]
        private List<AnnotationLocated> _locatedAnnotations;
        public List<AnnotationLocated> LocatedAnnotations
        {
            get { return _locatedAnnotations ?? (_locatedAnnotations = new List<AnnotationLocated>()); }
            set { _locatedAnnotations = value; }
        }

        public BitmapImage Bitmap { get; set; }

        #endregion
    }
}
