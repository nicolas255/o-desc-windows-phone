﻿using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class RealContext : PhysicalThing
    {
        #region Properties definitions

        [DataMember]
        private string _description;
        public string Description
        {
            get { return _description ?? (_description = ""); }
            set {
                ChangeProperty(ref _description, value);
            }
        }

        [DataMember]
        private Address _address;
        public Address Address
        {
            get { return _address ?? (_address = new Address()); }
            set
            {
                ChangeProperty(ref _address, value);
            }
        }

        #endregion
    }
}
