﻿using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class AnnotationLocated : Annotation
    {
        #region Properties definitions

        [DataMember]
        private double _positionX = -1;
        public double PositionX
        {
            get { return _positionX; }
            set { _positionX = value; }
        }

        [DataMember]
        private double _positionY = -1;
        public double PositionY
        {
            get { return _positionY; }
            set { _positionY = value; }
        }

        #endregion
    }
}
