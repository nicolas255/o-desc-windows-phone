﻿using System;
using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class Annotation
    {
        #region Properties definitions

        [DataMember]
        private string _note;
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        [DataMember]
        private DateTime _dateCreation;
        public DateTime DateCreation
        {
            get { return _dateCreation; }
            set { _dateCreation = value; }
        }

        #endregion

        public Annotation()
        {
            DateCreation = DateTime.Now;
            Note = "";
        }
    }
}
