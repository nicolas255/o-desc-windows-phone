﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class Resource : PhysicalThing
    {
        #region Properties definitions

        [DataMember]
        private List<Annotation> _annotations;
        public List<Annotation> Annotations
        {
            get { return _annotations ?? (_annotations = new List<Annotation>()); }
            set { _annotations = value; }
        }

        #endregion
    }
}
