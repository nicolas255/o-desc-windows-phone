﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace O_desc.Models
{
    [DataContract]
    public class Address : INotifyPropertyChanged
    {
        #region Properties definitions

        [DataMember]
        private string _streetLine1;
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                ChangeProperty(ref _streetLine1, value);
            }
        }

        [DataMember]
        private string _streetLine2;
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                ChangeProperty(ref _streetLine2, value);
            }
        }

        [DataMember]
        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                ChangeProperty(ref _city, value);
            }
        }

        [DataMember]
        private string _zipCode;
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                ChangeProperty(ref _zipCode, value);
            }
        }

        [DataMember]
        private string _country;
        public string Country
        {
            get { return _country; }
            set
            {
                ChangeProperty(ref _country, value);
            }
        }

        #endregion

        public Address()
        {
            StreetLine1 = "";
            StreetLine2 = "";
            City = "";
            ZipCode = "";
            Country = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] string propertyName = "")
        {
            if (Equals(attribute, value)) return;
            attribute = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(propertyName);
        }
    }
}
